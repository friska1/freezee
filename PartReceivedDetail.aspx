﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PartReceivedDetail.aspx.cs" Inherits="Delogi.Form.UI.Warehouse.Transaction.PartReceivedDetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/UserControl/GeneralTitleHeader.ascx" TagPrefix="uc1" TagName="GeneralTitleHeader" %>
<%@ Register Src="~/UI/UserControl/UC_GeneralSearching.ascx" TagPrefix="uc1" TagName="UC_GeneralSearching" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../../Style/Css/GeneralStyle.css" rel="stylesheet" />
    <link href="../../../Style/Additional/bootstrap.css" rel="stylesheet" />
    <script src="Scripts/html5-qrcode.min.js"></script>
</head>
<body class="genBody" oncontextmenu="return false;">
    <form id="form1" runat="server" enctype="multipart/form-data">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <uc1:GeneralTitleHeader runat="server" ID="GeneralTitleHeader" />
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                            <div class="float-right">
                                <asp:Label ID="lblRefNo" runat="server" CssClass="general"></asp:Label>
                                <asp:Button ID="btnClose" runat="server" CssClass="btnExitIcon" OnClick="btnClose_Click" Style="margin-right: 5px" />
                            </div>
                        </div>
                    </div>

                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" CollapseControlID="panelAdvanceHead"
                        CollapsedImage="~/Style/Image/Main/Misc/collapse.jpg" ExpandControlID="panelAdvanceHead"
                        ExpandedImage="~/Style/Image/Main/Misc/expand.jpg" TargetControlID="panelAdvanceHead"
                        ImageControlID="GeneralFillImage" runat="server"></cc1:CollapsiblePanelExtender>

                    <asp:Panel ID="panelAdvanceHead" runat="server" CssClass="general">
                        <div>
                            <asp:ImageButton ID="GeneralFillImage" runat="server" ImageUrl="~/Style/Image/Main/Misc/expand.jpg" />
                            <asp:Label ID="Label49" runat="server" Text="Header Information" Font-Bold="True" CssClass="general"></asp:Label>
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="panelAdvance" runat="server">
                        <table width="100%" class="genTable">
                            <tr>
                                <td width="13%">RefNo</td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:Label ID="LbRefno" runat="server" CssClass="general"></asp:Label>
                                </td>

                                <td width="13%">Warehouse </td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:DropDownList ID="ddWhouse" runat="server" CssClass="general form-control">
                                    </asp:DropDownList>
                                </td>

                            </tr>
                            <tr>
                                <td width="13%">Employee No</td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:Label ID="LbEmpNo" runat="server" CssClass="general"></asp:Label>
                                    &nbsp;&nbsp;
                                </td>
                                <td width="13%">DO No. </td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:TextBox ID="TbDoNo" runat="server" CssClass="general form-control" Width="120px"></asp:TextBox>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td width="13%">Employee Name</td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:Label ID="LbEmpName" runat="server" CssClass="general"></asp:Label>
                                </td>
                                <td width="13%">DO Date </td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:TextBox ID="TbDoDate" runat="server" CssClass="general form-control" Width="120px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server"
                                        CssClass="gencalBlue" Enabled="True" Format="yyyy-MM-dd"
                                        PopupButtonID="TbDoDate" TargetControlID="TbDoDate"></cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td width="13%">DO Input Date </td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:Label ID="LbDOInputDate" runat="server" CssClass="general"></asp:Label>
                                </td>
                                <td width="13%">Shipping No</td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:TextBox ID="tbShipNo" runat="server" CssClass="general form-control" Width="120px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>



                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender4" CollapseControlID="panelAdvanceHead"
                        CollapsedImage="~/Style/Image/Main/Misc/collapse.jpg" ExpandControlID="panelAdvanceHead"
                        ExpandedImage="~/Style/Image/Main/Misc/expand.jpg" TargetControlID="panel1"
                        ImageControlID="ImageButton2" runat="server"></cc1:CollapsiblePanelExtender>

                    <asp:Panel ID="panel1" runat="server" CssClass="general">
                        <div>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Style/Image/Main/Misc/expand.jpg" />
                            <asp:Label ID="Label2" runat="server" Text="Supplier Information" Font-Bold="True" CssClass="general"></asp:Label>
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="panel2" runat="server">
                        <table width="100%" class="genTable">
                            <tr>
                                <td width="13%">Vendor Code</td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:TextBox ID="tbSupplierCode" runat="server" CssClass="general"></asp:TextBox>
                                    <asp:HiddenField ID="hfSuppID" runat="server" />
                                </td>

                                <td width="13%">PO NO </td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:TextBox ID="tbPONO" runat="server" CssClass="general"></asp:TextBox>

                                </td>

                            </tr>
                            <tr>
                                <td width="13%">Vendor Name</td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:Label ID="lbSuppName" runat="server" CssClass="general"></asp:Label>
                                    &nbsp;&nbsp;
                                </td>
                                <td width="13%">PO Date</td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:Label ID="lbPODate" runat="server" CssClass="general"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td width="13%">Vendor Address</td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:Label ID="lbSuppAddress" runat="server" CssClass="general"></asp:Label>
                                </td>
                                <td width="13%">Packing </td>
                                <td width="1%">:</td>
                                <td width="27%">
                                    <asp:Label ID="lblPacking" runat="server" CssClass="general"></asp:Label>
                                </td>

                            </tr>
                            <tr>
                                <td width="13%"></td>
                                <td width="1%"></td>
                                <td width="27%"> <asp:Label ID="lbStatus" runat="server" Text='<%# Bind("Employee_Name") %>' ForeColor="Green" ></asp:Label>
                                       <br /> <asp:Image  runat="server" ID="imgFoto" Width="120px" Height="150px"/><br />                              
</td>
                                <td width="50%" colspan="3">
                                    <asp:Button ID="btnsave" runat="server" CssClass="btnBlue" Height="20px" OnClick="btnsave_Click" Text="Save" Width="60px" />
                                    &nbsp;<asp:Button ID="Btnupdate" runat="server" CssClass="btnBlue" Height="20px" OnClick="Btnupdate_Click" Text="Update" Visible="false" Width="60px" />
                                    &nbsp;
                                    <asp:Button ID="BtSubmit" runat="server" Text="Submit" CssClass="btnBlue" OnClick="BtSubmit_Click" Width="60px" Height="20px" />
                                      &nbsp; <asp:Button ID="ScanQR" runat="server" Text="Scan QR by Camera" OnClick="ScanQR_Click" CssClass="btnBlue"  />
                                           <asp:Label ID="lblEnterCount" CssClass="general" Text="" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                    <%-- ************************************************************************************************************************************--%>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" CollapseControlID="panelAdvHead" CollapsedImage="~/Style/Image/Main/Misc/collapse.jpg" ExpandControlID="panelAdvHead" ExpandedImage="~/Style/Image/Main/Misc/expand.jpg" 
                        ImageControlID="ImageButton1" TargetControlID="panelAdvHead"></cc1:CollapsiblePanelExtender>
                    <asp:Panel ID="panelAdvHead" runat="server" CssClass="general">
                        <div>
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Style/Image/Main/Misc/expand.jpg" />
                            <asp:Label ID="Label1" runat="server" CssClass="general" Font-Bold="True" Text="Product Receive"></asp:Label>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelDetail" runat="server" Visible="true">
                        <asp:GridView ID="Gv_Detail" runat="server" AllowPaging="false" AllowSorting="True" AutoGenerateColumns="false" CssClass="mGrid" OnPageIndexChanging="Gv_Detail_PageIndexChanging" OnRowCancelingEdit="Gv_Detail_RowCancelingEdit" OnRowCommand="Gv_Detail_RowCommand" OnRowDataBound="Gv_Detail_RowDataBound" OnRowEditing="Gv_Detail_RowEditing" OnRowUpdating="Gv_Detail_RowUpdating" OnSorting="Gv_Detail_Sorting" PageSize="10" ShowFooter="True" Width="100%">
                            <PagerStyle CssClass="pgr" />
                            <HeaderStyle ForeColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server" CssClass="general">
                                        </asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="4%" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ReqmDetID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LblReqmDetID" runat="server" CssClass="general" Text='<%# Bind("ReqmDetID")%>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ReqmID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LblReqmID" runat="server" CssClass="general" Text='<%# Bind("ReqmID")%>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SAP PART NO">
                                    <ItemTemplate>
                                        <asp:TextBox ID="TbItemsCode" runat="server" CssClass="general form-control" Text='<%# Bind("itemsCode") %>' Width="120px"></asp:TextBox>
                                        <asp:Label ID="lbItemsID" runat="server" CssClass="general" Text='<%# Bind("itemsID")%>' Visible="false"> </asp:Label>
                                        <asp:Button ID="btnItemsCode" runat="server" CommandName="ItemSearch" CssClass="btnViewIcon" Visible="false" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TbItemsCode" runat="server" CssClass="general form-control" Text='<%# Bind("itemsCode") %>' Width="120px">
                                        </asp:TextBox>
                                        <asp:Label ID="lbItemsID" runat="server" CssClass="general" Text='<%# Bind("itemsID")%>' Visible="false"> </asp:Label>
                                        <asp:Button ID="btnItemsCode" runat="server" CommandName="ItemSearch" CssClass="btnViewIcon" />
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TbItemsCode" runat="server" CssClass="general form-control" Width="120px" OnTextChanged="TbItemsCode_TextChanged" AutoPostBack="false"></asp:TextBox>
                                        <asp:Button ID="btnItemsCode" runat="server" CommandName="ItemSearch" CssClass="btnViewIcon" />
                                        <asp:Label ID="lbItemsID" runat="server" CssClass="general" Text='<%# Bind("itemsID")%>' Visible="false"> </asp:Label>
                                       
                                    </FooterTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="200px" />
                                    <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PART NAME" SortExpression="itemsName">
                                    <ItemTemplate>
                                        <asp:Label ID="LblitemsName" runat="server" CssClass="general" Text='<%# Bind("itemsName") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="LblitemsName" runat="server" CssClass="general" Text='<%# Bind("itemsName") %>'> 
                                        </asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="LblitemsName" runat="server" CssClass="general"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PART DESC" SortExpression="partDesc">
                                    <ItemTemplate>
                                        <asp:Label ID="LblitemsNameAlias" runat="server" CssClass="general" Text='<%# Bind("partDesc") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="LblitemsNameAlias" runat="server" CssClass="general" Text='<%# Bind("partDesc") %>'> 
                                        </asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="LblitemsNameAlias" runat="server" CssClass="general"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="VENDOR NAME" SortExpression="SuppName" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LblSuppName" runat="server" CssClass="general" Text='<%# Bind("SuppName") %>'> 
                                        </asp:Label>
                                        <asp:Label ID="LblSuppID" runat="server" CssClass="general" Text='<%# Bind("SuppID")%>' Visible="false"> </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="LblSuppName" runat="server" CssClass="general" Text='<%# Bind("SuppName") %>'> 
                                        </asp:Label>
                                        <asp:Label ID="LblSuppID" runat="server" CssClass="general" Text='<%# Bind("SuppID")%>' Visible="false"> </asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="LblSuppName" runat="server" CssClass="general"></asp:Label>
                                        <asp:Label ID="LblSuppID" runat="server" CssClass="general" Text='<%# Bind("SuppID")%>' Visible="false"> </asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PART UNIT" SortExpression="goodsUnit">
                                    <ItemTemplate>
                                        <asp:Label ID="LblUnit" runat="server" CssClass="general" Text='<%# Bind("goodsUnit") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="LblUnit" runat="server" CssClass="general" Text='<%# Bind("goodsUnit") %>'> 
                                        </asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="LblUnit" runat="server" CssClass="general"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="QTY" SortExpression="SBQ">
                                    <ItemTemplate>
                                        <asp:Label ID="LblSBQ" runat="server" CssClass="general" Text='<%# Bind("SBQ") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="LblSBQ" runat="server" CssClass="general" Text='<%# Bind("SBQ") %>'> 
                                        </asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="LblSBQ" runat="server" CssClass="general"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="LABEL TYPE" SortExpression="labelTypeText">
                                    <ItemTemplate>
                                        <asp:Label ID="LbllabelType" runat="server" CssClass="general" Text='<%# Bind("labelTypeText") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="LbllabelType" runat="server" CssClass="general" Text='<%# Bind("labelTypeText") %>'> 
                                        </asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="LbllabelType" runat="server" CssClass="general"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="RECQTY" SortExpression="RecQty">
                                    <ItemTemplate>
                                        <asp:Label ID="LblRecQty" runat="server" CssClass="general" Text='<%# Bind("RecQty") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="ETbRecQty" runat="server" CssClass="general form-control" Text='<%# Bind("RecQty") %>' Width="30px">
                                        </asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTbRecQty" runat="server" CssClass="general form-control" Width="30px"></asp:TextBox>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="LOT NO" SortExpression="LotNo">
                                    <ItemTemplate>
                                        <asp:Label ID="LblLotNo" runat="server" CssClass="general" Text='<%# Bind("LotNo") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="ETbLotNo" runat="server" CssClass="general form-control" Text='<%# Bind("LotNo") %>' Width="70px">
                                        </asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTbLotNo" runat="server" CssClass="general form-control" Width="70px"></asp:TextBox>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BATCH NO" SortExpression="batchNo">
                                    <ItemTemplate>
                                        <asp:Label ID="LblbatchNo" runat="server" CssClass="general" Text='<%# Bind("batchNo") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="ETbbatchNo" runat="server" CssClass="general form-control" Text='<%# Bind("batchNo") %>' Width="70px">
                                        </asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTbbatchNo" runat="server" CssClass="general form-control" Width="70px"></asp:TextBox>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="EXPIRED DATE" SortExpression="ExpiredDate">
                                    <ItemTemplate>
                                        <asp:Label ID="LblExpiredDate" runat="server" CssClass="general" Text='<%# Bind("ExpiredDate") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="ETbExpiredDate" runat="server" CssClass="general form-control" Text='<%# Bind("ExpiredDate") %>' Width="120px"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendaETbExpiredDate" runat="server" CssClass="gencalBlue" Enabled="True" Format="yyyy-MM-dd" PopupButtonID="ETbExpiredDate" TargetControlID="ETbExpiredDate"></cc1:CalendarExtender>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTbExpiredDate" runat="server" CssClass="general form-control" Width="120px"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendaFTbExpiredDate" runat="server" CssClass="gencalBlue" Enabled="True" Format="yyyy-MM-dd" PopupButtonID="FTbExpiredDate" TargetControlID="FTbExpiredDate"></cc1:CalendarExtender>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="REMARK" SortExpression="remark">
                                    <ItemTemplate>
                                        <asp:Label ID="Lblremark" runat="server" CssClass="general" Text='<%# Bind("remark") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="ETbremark" runat="server" CssClass="general form-control" Text='<%# Bind("remark") %>' Width="120px">
                                        </asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="FTbremark" runat="server" CssClass="general form-control" Width="120px"></asp:TextBox>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="EDIT">
                                    <ItemTemplate>
                                        <asp:Button ID="btnViewEdit" runat="server" CausesValidation="False" CommandName="edit" CssClass="btnEditIcon" ToolTip="Edit" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Button ID="btnViewUpdate" runat="server" CausesValidation="True" CommandName="Update" CssClass="btnSubmitIcon" ToolTip="Update" />
                                        <asp:Button ID="btnViewReject" runat="server" CausesValidation="true" CommandName="Cancel" CssClass="btnRejectIcon" ToolTip="Cancel" />
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DELETE">
                                    <ItemTemplate>
                                        <asp:Button ID="btndelete" runat="server" CausesValidation="False" CommandName="del" CssClass="btnDeleteIcon" ToolTip="Delete" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Button ID="btnsave_RatShare" runat="server" CausesValidation="false" CommandName="save" CssClass="btnBlue" Text="Save" ToolTip="Save" />
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </asp:Panel>
                    <%--------------------------------------------------------------------------------%>
                    <br />
                    <%--------------------------------------------------------------------------------%>
                    <asp:Panel ID="panelSearch" runat="server" BackColor="#FFFFFF" GroupingText="Asset Search" Visible="False">
                        <table width="100%">
                            <tr>
                                <td>Filter By&nbsp; &nbsp;
                                    <asp:DropDownList ID="ddSearch" runat="server" CssClass="general form-control" Font-Names="Tahoma" Font-Size="8pt" Width="140px">
                                        <asp:ListItem Value=""></asp:ListItem>
                                        <asp:ListItem Value="A.itemsCode">Material Code</asp:ListItem>
                                        <asp:ListItem Value="A.itemsName">Material Name</asp:ListItem>
                                        <asp:ListItem Value="b.suppName">Supplier</asp:ListItem>
                                        <asp:ListItem Value="a.itemsNameAlias">Material Desc</asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="tbSearch" runat="server" CssClass="general form-control" Font-Names="Tahoma" Font-Size="8pt" Width="200px"> </asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnSearch" runat="server" CssClass="btnBlue" Height="20px" OnClick="btnSearch_Click" Text="Search" Width="70px" />
                                    &nbsp; &nbsp;
                                    <asp:Button ID="Button1" runat="server" CssClass="btnBlue" Height="20px" OnClick="btnCancel_Click" Text="Close" Width="70px" />
                                </td>
                            </tr>
                        </table>
                        <asp:GridView ID="GVGoodslabelSearch" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="false" CssClass="mGrid" OnPageIndexChanging="GVGoodslabelSearch_PageIndexChanging" OnRowCommand="GVGoodslabelSearch_RowCommand" OnRowDataBound="GVGoodslabelSearch_RowDataBound" OnSorting="GVGoodslabelSearch_Sorting">
                            <Columns>
                                <asp:TemplateField HeaderText="No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="2%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="itemsID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lbitemsID" runat="server" Text='<% #Bind("itemsID")%>'></asp:Label>
                                        <asp:Label ID="lbwhouseID" runat="server" Text='<% #Bind("whouseID")%>'></asp:Label>
                                        <asp:Label ID="lblsuppName" runat="server" Text='<% #Bind("suppName")%>'></asp:Label>
                                        <asp:Label ID="LblSuppID" runat="server" Text='<% #Bind("suppID")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SAP PART NO">
                                    <ItemTemplate>
                                        <asp:Label ID="lbitemsCode" runat="server" Text='<% #Bind("itemsCode")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PART NAME">
                                    <ItemTemplate>
                                        <asp:Label ID="lbitemsName" runat="server" Text='<% #Bind("itemsName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PART DESC">
                                    <ItemTemplate>
                                        <asp:Label ID="lbitemDesc" runat="server" Text='<% #Bind("itemsNameAlias")%>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="WAREHOUSE" SortExpression="whouseName">
                                    <ItemTemplate>
                                        <asp:Label ID="lbwhouseName" runat="server" Text='<% #Bind("whouseName")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CURRENT QTY" SortExpression="currentQty">
                                    <ItemTemplate>
                                        <asp:Label ID="lbcurrentQty" runat="server" Text='<% #Bind("nowStock")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PART UNIT" SortExpression="goodsUnit">
                                    <ItemTemplate>
                                        <asp:Label ID="lbgoodsUnit" runat="server" Text='<% #Bind("goodsUnit")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="QTY" SortExpression="SBQ">
                                    <ItemTemplate>
                                        <asp:Label ID="lbSBQ" runat="server" Text='<% #Bind("SBQ")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="LABEL TYPE" SortExpression="labelTypeText">
                                    <ItemTemplate>
                                        <asp:Label ID="lblabelType" runat="server" Text='<% #Bind("labelTypeText")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PROJECT" SortExpression="projName">
                                    <ItemTemplate>
                                        <asp:Label ID="lbprojName" runat="server" Text='<% #Bind("projName")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ADD">
                                    <ItemTemplate>
                                        <asp:Button ID="Buttonadd" runat="server" CommandName="addGoods" CssClass="btnBlue" Text="Add" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle Width="3%" />
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="pgr" />
                            <HeaderStyle ForeColor="White" />
                        </asp:GridView>
                    </asp:Panel>
                    <%--------------------------------------------------------------------%>
                    <br />
                    <div>
                        <div style="width: 500px" id="qr-reader">
                        </div>


                    <asp:HiddenField ID="hf_empno" runat="server" />
                    <asp:HiddenField ID="hf_empname" runat="server" />
                    <asp:HiddenField ID="hf_loginTime" runat="server" />
                    <asp:HiddenField ID="hf_deptID" runat="server" />
                    <asp:HiddenField ID="hf_HuroDeptID" runat="server" />
                    <asp:HiddenField ID="hf_userlevel" runat="server" />
                    <asp:HiddenField ID="hf_taskID" runat="server" />
                    <asp:HiddenField ID="hf_taskName" runat="server" />
                    <asp:HiddenField ID="hf_menuID" runat="server" />
                    <asp:HiddenField ID="hf_userID" runat="server" />
                    <asp:HiddenField ID="hf_dept" runat="server" />
                    <asp:HiddenField ID="hf_divisi" runat="server" />
                    <asp:HiddenField ID="hf_HuroDivisiID" runat="server" />
                    <asp:HiddenField ID="hf_Add" runat="server" />
                    <asp:HiddenField ID="hf_Update" runat="server" />
                    <asp:HiddenField ID="hf_delete" runat="server" />
                    <asp:HiddenField ID="hf_ro" runat="server" />
                    <asp:HiddenField ID="hf_AllDataBool" runat="server" />
                    <asp:HiddenField ID="hf_PageNo" runat="server" />
                    <asp:HiddenField ID="hf_advDetID" runat="server" />
                    <asp:HiddenField ID="hf_modeview" runat="server" />
                    <asp:HiddenField ID="hf_ipaddress" runat="server" />
                    <asp:HiddenField ID="hfappstep" runat="server" />
                    <asp:HiddenField ID="hf_Page" runat="server" />
                    <asp:HiddenField ID="hf_Row" runat="server" />
                    <asp:HiddenField ID="hf_ID" runat="server" />
                    <asp:HiddenField ID="hf_status" runat="server" />
                    <asp:HiddenField ID="hf_tabIndex" runat="server" />
                    <asp:HiddenField ID="hf_appstep" runat="server" />
                    <asp:HiddenField ID="hf_sts" runat="server" />
                    <asp:HiddenField ID="hf_stsGV" runat="server" />
                    <asp:HiddenField ID="hf_stsBtn" runat="server" />
                    <asp:HiddenField ID="hf_onlineID" runat="server" />
                    <asp:HiddenField ID="hfBtnUploadExcel" runat="server" />
                    <asp:HiddenField ID="hf_FileNameToUploadDoc" runat="server" />
                    <asp:HiddenField ID="hfmodulpicture" runat="server" />
                    <asp:HiddenField ID="hf_waktu" runat="server" />
                    <asp:HiddenField ID="Hf_FolderDownloadpic" runat="server" />
                    <asp:HiddenField ID="HF_FolderUploadpic" runat="server" />
                    <asp:HiddenField ID="hfmodulDoc" runat="server" />
                    <asp:HiddenField ID="Hf_FolderDownloadDoc" runat="server" />
                    <asp:HiddenField ID="HF_FolderUploadDoc" runat="server" />
                    <asp:HiddenField ID="hf_errorCount" runat="server" />
                    <asp:HiddenField ID="hf_statusAttach" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UProUP" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0" EnableViewState="true">
                <ProgressTemplate>
                    <div id="Div1" align="center" valign="bottom" runat="server" class="genProgress">
                        <div id="Div2" align="center" valign="bottom" runat="server" class="genProgress2">
                            <img src="../../../Style/Image/Main/ProgressBar/loadingbar.gif" />
                            <br />
                            <span style="font-weight: bold">Loading</span>
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>

          <script type="text/javascript">
            var separCount = 0;
            var EnterCount = 0;
            var qrCodeScanner;
            qrCodeScanner = new Html5QrcodeScanner("qr-reader", {
                fps: 10, qrbox: 170, useBarCodeDetectorIfSupported: true
            });
            //$(document).ready(function () {
                

                qrCodeScanner.render(onScanSuccess);
            //});

            function onScanSuccess(decodedText, decodedResult) {

                const myArray = decodedText.split(";");
                let word = myArray[0];
                alert(word);
                CheckScanResult(word);
                //qrCodeScanner.clear();
            }
            
            $("#TbItemsCode").keypress(function (e) {
                var dept = $("#TbItemsCode").val();
                if (e.which == 13) {
                    EnterCount++;
                    if (EnterCount == 6) {
                        var mylabel = document.getElementById("lbItemsID");
                        var str = $("#TbItemsCode").val();

                        
                        $("#lbItemsID").text(str);
                        EnterCount = 0;
                        
                        // __doPostBack("TbItemsCode", "TextChanged");
                        CheckScanResult(str);
                        $("#TbItemsCode").val("");
                        

                    }
                    $("#lblEnterCount").text(EnterCount);
                    e.preventDefault();
                }
                else if (e.which == 59) {
                    separCount++;

                }
                else {
                }
            });

            function doback()
            {
                __doPostBack("Page", "Load");
            }
             function CheckScanResult (EmpNo){
                
                 var myEventID = document.getElementById('<%= hf_advDetID.ClientID %>').value
                 //var myAllow = document.getElementById<%--('<%=cbAllow.ClientID%>')--%>.value;
                 //var x = $("#checkbox").is(":checked");
                 var x = document.getElementById("cbAllow").checked

                 //alert(document.getElementById<%--('<%= hf_EventID.ClientID %>')--%>.value);
                $.ajax({
                    url: "EventHandlingAttendanceDetail.aspx/CheckPartisipant",
                    async: true,
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({ EmpNo: EmpNo, EventID: myEventID, AllowUnregist: x }),
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        var myRes = data.d.split('|')
                        //alert(myRes[0]);
                        if (myRes[0] == '[SUCCESS]')
                        {
                            var cookName = 'lastParti' + myEventID;
                            document.cookie = cookName + "=" + myRes[0] + "|" + EmpNo;

                            //alert(data.d); //Success | [...] | [...]
                            $("#lbStatus").text(data.d);
                            document.getElementById('lbStatus').setAttribute('style', 'color: green;');
                          
                            setTimeout(doback(),3000);
                         
                           
                         
                           //document.getElementById<%--('<%= hf_EventID.ClientID %>')--%>.setAttribute('style', 'color: green;');
                        }
                        else if (myRes[0] == '[EXISTING]') {
                            $("#lbStatus").text(data.d);

                            document.getElementById('lbStatus').setAttribute('style', 'color: orange;');
  
        
                            const img = document.getElementById('imgFoto');
                            img.setAttribute('src', '');
                        }

                        //else if (myRes[0] == '[UNREGISTER]') {
                        else{
                            $("#lbStatus").text(data.d);
                            
                            document.getElementById('lbStatus').setAttribute('style', 'color: red;');

                            //document.getElementById<%--('<%= hf_EventID.ClientID %>')--%>.setAttribute('style', 'color: red;');
                        }
                        //else alert('Unregistered...');
                        
                        

                    }, error: function (xhr, status, error) {
                        //var msg = JSON.parse(status);
                        alert('no data found ')

                        document.getElementById('lbStatus').setAttribute('style', 'color: orange;');
                        //document.getElementByI<%--d('<%= hf_EventID.ClientID %>')--%>.setAttribute('style', 'color: yellow;');
                    },
                });
            } 
        </script>


    </form>
</body>
</html>

